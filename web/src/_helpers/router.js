import Vue from 'vue';
import Router from 'vue-router';

import HomePage from '../home/HomePage'
import LoginPage from '../login/LoginPage'
import ResourcePage from '../resource/ResourcePage'
import CreateResource from '../resource/CreateResource'

Vue.use(Router);

const loggedIn = localStorage.getItem('user');

export const router = new Router({
  mode: 'history',
  routes: [
    { path: '/', component: HomePage,meta: {loggedIn: true}, },
    { path: '/login', component: LoginPage,meta: {loggedIn: false}, },
    { path: '/resources', component: ResourcePage,meta: {loggedIn: true}, },
    { path: '/resources/create', component: CreateResource,meta: {loggedIn: true}, },

    // otherwise redirect to home
    { path: '*', redirect: '/' }
  ]
});

router.beforeEach((to, from, next) => {
  // redirect to login page if not logged in and trying to access a restricted page
  const publicPages = ['/login'];
  const authRequired = !publicPages.includes(to.path);

  if (authRequired && !loggedIn) {
    return next('/login');
  }

  next();
})