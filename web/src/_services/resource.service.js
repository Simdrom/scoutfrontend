import config from 'config';
import { authHeader } from '../_helpers';

export const resourceService = {
  getAll,
  getById,
  create,
  update,
  deleteById,
  deleteAll
};

function getAll(){
  const requestOptions = {
    method: 'GET',
    headers: authHeader()
  };

  return fetch(`${config.apiUrl}/api/resources`, requestOptions).then(handleResponse);

};

function getById(){

};

function create(name, content, author,description){
  const requestOptions = {
    method: 'POST',
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify({name, content, author,description})
  };

  return fetch(`${config.apiUrl}/api/resources`, requestOptions)
    .then(handleResponse)
    .then(resource => {
      return resource;
    });
};

function update(){

};

function deleteById(){

};

function deleteAll(){

};

function handleResponse(response) {
  return response.text().then(text => {
    const data = text && JSON.parse(text);
    if (!response.ok) {
      if (response.status === 401) {
        // auto logout if 401 response returned from api
        logout();
        location.reload(true);
      }

      const error = (data && data.message) || response.statusText;
      return Promise.reject(error);
    }

    return data;
  });
}
