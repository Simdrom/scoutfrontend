export const alert = {
  namespaced: true,
  state: {
    type: null,
    message: null,
    variant: null
  },
  actions: {
    success({commit}, message) {
      commit('success', message);
    },
    error({commit}, message) {
      commit('error', message);
    },
    clear({commit}, message) {
      commit('success', message);
    }
  },
  mutations: {
    success(state, message) {
      state.type = 'alert-success';
      state.message = message;
      state.variant = 'success';
    },
    error(state, message) {
      state.type = 'alert-danger';
      state.message = message;
      state.variant = 'danger';
    },
    clear(state) {
      state.type = null;
      state.message = null;
    }
  }
}
