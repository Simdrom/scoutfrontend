import {userService} from '../_services';
import {router} from '../_helpers';

const user = JSON.parse(localStorage.getItem('user'));
const initialState = user  ? {status: {loggedIn: true}, user}  : {status: {}, user: null};


export const authentication = {
  namespaced: true,
  state: {
    initialState,
    isLogged: false
  },
  actions: {
    login({dispatch, commit}, {username, password}) {
      commit('loginRequest', {username});

      userService.login(username, password)
        .then(
          user => {
            if (user.token === undefined && user === "No user found.") {
              commit('loginFailure', user);
              dispatch('alert/error', user, {root: true});
            } else {
              commit('loginSuccess', user);
              router.push('/');
            }
          },
          error => {
            commit('loginFailure', error);
            dispatch('alert/error', error, {root: true});
          }
        );
    },
    logout({commit}) {
      userService.logout();
      commit('logout');
    }
  },
  mutations: {
    loginRequest(state, user) {
      state.status = {loggingIn: true};
      state.user = user;
      state.isLogged = false;
    },
    loginSuccess(state, user) {
      state.status = {loggedIn: true};
      state.user = user;
      state.isLogged = true;
    },
    loginFailure(state) {
      state.status = {};
      state.user = null;
      state.isLogged = false;
    },
    logout(state) {
      state.status = {};
      state.user = null;
      state.isLogged = false;
    }
  }
}
