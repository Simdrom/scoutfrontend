import {resourceService} from '../_services';
import {router} from "../_helpers";

export const resources = {
  namespaced: true,
  state: {
    all: {}
  },
  actions: {
    getAll({commit}) {
      commit('getAllRequest');

      resourceService.getAll()
        .then(
          resources => commit('getAllSuccess', resources),
          error => commit('getAllFailure', error)
        );
    },
    create({ dispatch, commit }, {name, content, author,description}) {
      commit('getAllRequest');

      resourceService.create(name, content, author,description)
        .then(
          resource => {
            dispatch('alert/clear', resource, {root: true});
            commit('getAllSuccess', resource);
            dispatch('alert/success', resource, {root: true});
            router.push('/');
          },
          error => {
            commit('getAllFailure', error);
            dispatch('alert/error', error, { root: true });
          }
        );
    },
  },
  mutations: {
    getAllRequest(state) {
      state.all = {loading: true};
    },
    getAllSuccess(state, resources) {
      state.all = {items: resources};
    },
    getAllFailure(state, error) {
      state.all = {error};
    }
  }
}