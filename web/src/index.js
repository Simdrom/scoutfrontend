import Vue from 'vue';
import { store } from './_store';
import { router } from './_helpers';
import BootstrapVue from 'bootstrap-vue';
import App from './app/App';

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
Vue.use(BootstrapVue);

new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App)
});